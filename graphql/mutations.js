import { gql } from '@apollo/client';


export const CREATE_POST = gql`
    mutation CreatePost($body: String!, $imageUrls: [String], $hashtags: [String]!) {
        createPost(body: $body, imageUrls: $imageUrls, hashtags: $hashtags) {
            id
        }
    }`;
export const LOGIN = gql`
    mutation Login($google_id: String!, $accessToken: String!) {
        login(google_id: $google_id, accessToken: $accessToken) {
            username
            email
            token
        }
    }`;

export const LIKE_POST = gql`
    mutation LikePost($parentId: ID!) {
        likePost(parentId: $parentId) {
            id
            type
            user
        }
    }
`

export const CREATE_COMMENT = gql`
mutation createComment($postId: ID!, $body: String!) {
    createComment(postId: $postId, body: $body) {
    id
    type
    createdAt
        body
        deleted
        likeCount
        likes {
        actionOn
        id
        type
        createdAt
        user
        username
        }
    body
}
}
`

export const LIKE_COMMENT = gql`
    mutation likeComment($parentId: ID!) {
        likeComment(parentId: $parentId) {
            id
            actionOn
            type
            createdAt
            user
            username
        }
    }
`

export const UPLOAD_FILE = gql`
    mutation($file: Upload!) {
        uploadFile(file: $file) {
            uri
        }
    }
`

export const DELETE_COMMENT = gql`
    mutation DeleteCommentMutation($deleteCommentParentId: ID!, $deleteCommentCommentId: ID!) {
        deleteComment (
            parentId: $deleteCommentParentId
            commentId: $deleteCommentCommentId
        )
    }  
`

export const DELETE_POST = gql`
    mutation DeletePostMutation($deletePostPostId: ID!) {
        deletePost(postId: $deletePostPostId)
    }
`
