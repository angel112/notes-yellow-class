import { gql } from '@apollo/client';


export const get_posts = gql`
        query GetPosts($pageSize: Int, $after: String) {
            getPosts(pageSize: $pageSize, after: $after){
                cursor
                hasMore
                posts {
                    id
                    user {
                        id
                        email
                        username
                        picture
                        createdAt
                    }
                    body
                    createdAt
                    imageUrls
                    hashtags
                    likes {
                        id
                        actionOn
                        createdAt
                        user
                        username
                    }
                    likeCount
                }
            }
        }
        `;

export const get_post = gql`
    query getPost($postId: ID!) {
        getPost(postId: $postId) {
            id
            user {
                id
                email
                username
                picture
                createdAt
            }
            body
            createdAt
            imageUrls
            hashtags
            likes {
                id
                actionOn
                createdAt
                user
                username
            }
            likeCount
        }
        
    }
`;

export const get_user = gql`
        query getUser($userId: ID!) {
                getUser(userId: $userId) {
                    id
                    username
                    picture
                    email
                    createdAt
                }
        }
    `;

export const GET_LIKES_ON_POST = gql`
        query GetLikesOnPost($postId: ID!) {
            getLikesOnPost(postId: $postId) {
                type
                user
            }
        }
`

export const get_comments = gql`
    query getComments($postId: ID!, $pageSize: Int, $after: String) {
        getComments(postId: $postId, pageSize: $pageSize, after: $after) {
        cursor
        hasMore
        comments {
            id
            actionOn
            type
            createdAt
            user {
            username
            id
            picture
            createdAt
            }
            body
            deleted
            likeCount
            likes {
            actionOn
            id
            type
            createdAt
            user
            username
            }
        }
        }
    }
`;

export const get_likes_on_comment = gql`
    query getLikesOnComment($getLikesOnCommentCommentId: ID!) {
        getLikesOnComment(commentId: $getLikesOnCommentCommentId) {
            id
            actionOn
            type
            createdAt
            user
            username
        }
    }
`;

export const get_posts_by_user = gql`
    query getPostsByUser($userId: ID!, $pageSize: Int, $after: String) {
        getPostsByUser(userId: $userId, pageSize: $pageSize, after: $after) {
        hasMore
        cursor
        posts {
            id
            user {
            id
            google_id
            email
            username
            given_name
            family_name
            picture
            createdAt
            }
            body
            createdAt
            imageUrls
            hashtags
            deleted
            likes {
            id
            actionOn
            type
            createdAt
            user
            username
            }
            likeCount
        }
        }
    }
`