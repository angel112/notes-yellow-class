import React, { useState, useEffect, Fragment } from 'react'
import Head from 'next/head'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { createUploadLink } from 'apollo-upload-client';
import { ToastProvider } from 'react-toast-notifications';
//components
import Layout from "components/layout";
import Body from "components/Body"

const BACKEND_URL = process.env.NEXT_PUBLIC_BACKEND_URL;

export default function Home() {
  //useState hooks
  const [authToken, setToken] = useState('');

  //useEffect hooks
  useEffect(() => {
    const token = window.localStorage.getItem("yc_jwt");
    setToken(token);
  }, [setToken])

  //creating apollo client configurations
  const client = new ApolloClient({
    link: createUploadLink({
      uri: `${BACKEND_URL}graphql`,
      headers: {
        Authorization: `Bearer ${authToken}`,
      }
    }),
    cache: new InMemoryCache()
  });

  return (
    <ApolloProvider client={client}>
      <ToastProvider
        autoDismiss
        autoDismissTimeout={6000}
        placement="top-center">
        <Fragment>
          <Head>
            <title>Home</title>
          </Head>
          <Layout>
            <Body />
          </Layout>
        </Fragment >
      </ToastProvider>
    </ApolloProvider>
  )
}

