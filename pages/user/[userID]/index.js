import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Layout from 'components/layout';
import UserInfo from 'components/UserInfo';
import { ApolloProvider, ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import styled from 'styled-components';
import RenderPostsByUser from 'components/RenderPostsByUser';
import { ToastProvider } from 'react-toast-notifications';



let authToken = '';
const BACKEND_URL = process.env.NEXT_PUBLIC_BACKEND_URL;

const createApolloClient = (authToken) => {
    return new ApolloClient({
        link: new HttpLink({
            uri: `${BACKEND_URL}/graphql`,
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }),
        cache: new InMemoryCache()
    });
};

const SingleUser = () => {
    //useState hook
    const [query, setQuery] = useState("");

    //router hook
    const router = useRouter();


    //useEffect hook
    useEffect(() => {
        //getting the query from the route
        const { userID } = router.query;
        setQuery(userID);
        const token = window.localStorage.getItem("yc_jwt");
        if (token) {
            authToken = token;
        }
    }, [])


    return (
        <React.Fragment>
            <ApolloProvider client={createApolloClient(authToken)}>
                <ToastProvider
                    autoDismiss
                    autoDismissTimeout={6000}
                    placement="top-center">
                    <Head>
                        <title>View user</title>
                    </Head>
                    <Layout>
                        <ContainerUserContents>
                            <UserInfo userID={query} />
                            <PostsWrapper>
                                <RenderPostsByUser userID={query} />
                            </PostsWrapper>
                        </ContainerUserContents>
                    </Layout>
                </ToastProvider>
            </ApolloProvider>
        </React.Fragment>
    )
}

export default SingleUser;

//styled-components

const ContainerUserContents = styled.div`
    margin-top: 6rem;
    display: flex;
    flex-direction: column;
`

const PostsWrapper = styled.div`
    margin-top: 50px;
    padding: 5px;
    display: flex;
    flex-direction: column;
`