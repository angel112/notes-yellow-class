import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { ApolloProvider, ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { createPersistedQueryLink } from 'apollo-link-persisted-queries';
import { createUploadLink } from 'apollo-upload-client';
import { ToastProvider } from 'react-toast-notifications';
//components
import Layout from 'components/layout';
import SinglePost from 'components/SinglePost';
//helper functions
import colors from 'components/utils/colors';


let authToken = '';
const BACKEND_URL = process.env.NEXT_PUBLIC_BACKEND_URL;

const createApolloClient = (token) => {
    // console.log("AuthToken", token);
    return new ApolloClient({
        link: createPersistedQueryLink().concat(createUploadLink({
            uri: `${BACKEND_URL}/graphql`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        })),
        cache: new InMemoryCache(),
    });
};

const FullPost = () => {
    useEffect(() => {
        const token = window.localStorage.getItem("yc_jwt");
        if (token) authToken = token;
    }, [])
    const client = createApolloClient(authToken);
    const router = useRouter();
    const { pid } = router.query;

    return (
        <>
            <ApolloProvider client={client}>
                <ToastProvider
                    autoDismiss
                    autoDismissTimeout={6000}
                    placement="top-center">
                    <Head>
                        <title>View post</title>
                    </Head>
                    <Layout>
                        <ContainerPost>
                            <SinglePost pid={pid} />
                        </ContainerPost>
                    </Layout>
                </ToastProvider>
            </ApolloProvider>
        </>
    )
}

export default FullPost;

const ContainerPost = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 1rem;
    background: ${colors.neutral200};
    
`
