import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import LikeButton from '../components/LikeButton';
import { useQuery, useMutation } from '@apollo/client';
import { get_post } from '../graphql/queries';
import Author from './Author';
import colors from './utils/colors';
import Bone from './Bones';
import CommentSection from './CommentSection';
import checkAuth from './utils/auth-check';
import { DELETE_POST } from '../graphql/mutations.js';
import { MdDelete } from 'react-icons/md';
import { useToasts } from 'react-toast-notifications';
import Carousel from 'react-images';


const SinglePost = ({ pid }) => {
    //useState hooks
    const [post, setPost] = useState();
    const [likesCount, setLikesCount] = useState();
    const [liked, setLiked] = useState(false);
    const [showDeleteButton, setShowDeleteButton] = useState(false);

    //graphql hooks
    const [deletePost, { data, error }] = useMutation(DELETE_POST, {
        variables: {
            deletePostPostId: pid
        }
    });

    const { data: postData, loading: loadPost } = useQuery(get_post, {
        variables: {
            postId: pid
        }
    });

    //useState hooks
    useEffect(() => {
        if (post) {
            setLikesCount(post.likeCount);
            let { response } = checkAuth(window.localStorage.getItem("yc_jwt"));
            if (response) {
                if (post.likes.find(o => o.user == response.id)) {
                    setLiked(true);
                }
                if (post.user.id == response.id) {
                    setShowDeleteButton(true);
                }
            }
        }
    }, [post]);

    useEffect(() => {
        if (postData) {
            setPost(postData.getPost);
        }
    }, [loadPost, setPost]);

    //methods
    const handlePostDeletion = async () => {
        try {
            const res = await deletePost();
            window.location = '/';
        } catch (err) {
            console.log(err);
        }
    }

    const images = (urls) => {
        const res = [];
        urls.map(url => {
            res.push({ source: url });
        })
        console.log(res);
        return res;
    }

    return (post ?
        <PostContainer>
            <Post>
                <Author post={post} />
                <Body>
                    <Caption>{post.body}</Caption>
                    {post.imageUrl != "" ?
                        <Media>
                            <Carousel
                                views={images(post.imageUrls)}
                            />
                        </Media> : null}
                </Body>
                <br />
                <Actions>
                    <LikeButton setLiked={setLiked} liked={liked} setLikesCount={setLikesCount} likesCount={likesCount} postId={pid} />
                    {showDeleteButton ? <DeleteButton onClick={handlePostDeletion} /> : null}
                </Actions>
                <CommentSection postId={post.id} />
            </Post>
        </PostContainer> : <Bone />
    )
}

export default SinglePost;

//styled-components

const Post = styled.div`
    display: flex;
    flex-direction: column;
    border: 1px solid ${colors.neutral300};
    width: 650px;
    background: ${colors.neutral100};
    border-radius: 0.2rem;
    padding: 1.2rem;
    z-index: 3;
`

const PostContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    padding: 56px;
`

const Caption = styled.h3`
    font-family: 'IBM Plex Sans', sans-serif;
`

const Media = styled.div`
    margin-left: 48px;
    display: flex;
    justify-content: center;
    align-items: flex-start;
`
const Body = styled.div`
    display: flex;
    flex-direction: column;
    cursor: pointer;
`

const DeleteButton = styled(MdDelete)`
    color: ${colors.primary300};
    cursor: pointer;
    padding: 2px;
    align-self: flex-end;
    font-size: 2rem;
    border-radius: 4rem;
    transition: all ease-out 250ms;
    :hover {
        color: ${colors.neutral100};
        background: ${colors.primary300};
        filter: none;
    }
`

const Actions = styled.div`
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid ${colors.neutral400};
    padding-bottom: 0.5rem;
`