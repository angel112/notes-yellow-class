import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import { useQuery } from '@apollo/client';
import Link from 'next/link';
//graphql
import { get_user } from '../graphql/queries';
//helper functions
import colors from './utils/colors';


const LoggedIn = ({ Logout, logoutUser, userId }) => {
    //useState hooks
    const [user, setUser] = useState();

    //graphql hooks
    const { data, loading } = useQuery(get_user, { variables: { userId: userId.id } });

    //useState hooks
    useEffect(async () => {
        if (!loading) {
            setUser(data);
        }
    }, [loading]);

    //methods
    const logout = () => {
        window.localStorage.removeItem("yc_jwt");
        logoutUser('');
        window.location = '/';
    }

    return (user ?
        <React.Fragment>
            <Link href={`/user/${user.getUser.id}`}>
                <UserImage src={user.getUser.picture} alt="userImage" />
            </Link>
            <Logout onClick={logout}>Logout</Logout>
        </React.Fragment> : null
    )
}

export default LoggedIn;

//styled-components

const UserImage = styled.img`
    width: 39px;
    height: 39px;
    border: 3px solid ${colors.primary400};
    border-radius: 5rem;
    cursor: pointer;
    margin-left: 0.5rem;
`