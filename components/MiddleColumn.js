import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useQuery } from '@apollo/client';
import { useSpring, animated } from 'react-spring';
//components
import Post from './Post';
import Bone from './Bones';
import Pagination from './Pagination';
//graphql
import { get_posts } from '../graphql/queries';
//helper functions
import typography from './utils/typography';

const pageSize = 10;        //number of posts to be displayed in each page

const MiddleColumn = () => {
    //useState hooks
    const [more, setMore] = useState(false);
    const [cursor, setCursor] = useState();
    const [posts, setPosts] = useState(undefined);

    //graphql hooks
    const { data, loading, fetchMore } = useQuery(get_posts, {
        variables: {
            pageSize: pageSize
        },
        fetchPolicy: "network-and-cache"
    });

    //useSpring hooks
    const props = useSpring({
        opacity: 1,
        from: { opacity: 0 },
    });

    //useEffect hooks
    useEffect(() => {
        if (!loading) {
            setPosts(data?.getPosts.posts);
            setCursor(data?.getPosts.cursor);
            setMore(data?.getPosts.hasMore);
        }
    }, [loading]);

    //methods
    const onNext = async () => {                        //function to display next page in pagination
        if (more) {
            const response = await fetchMore({
                variables: {
                    after: cursor
                }
            })
            const data = [...posts, ...response.data.getPosts.posts];
            setPosts(data);                             //populate new posts 
            setCursor(response.data.getPosts.cursor);   //initialize new cursor in state
            setMore(response.data.getPosts.hasMore);
        }
    }
    return (
        <MiddleColumnContainer>
            <Header style={props}>Posts</Header>
            {!posts ? <Bone />
                : posts.map((item, index) =>
                    <Post className='post' key={index} i={index} item={item} setPosts={setPosts} posts={posts} />
                )}
            <Pagination onNext={onNext} more={more} />
        </MiddleColumnContainer>
    )
}

export default MiddleColumn;

//styled-components

const MiddleColumnContainer = styled.div`
    display: flex;
    flex-flow: column;
    margin: 10px;
    border-radius: 20px;
    padding: ${typography.para};
    @media (max-width: 700px) {
        width: 100vw;
        padding: 0;
    }
`

const Header = styled(animated.h1)`
    /* font-family: 'Archivo Black', sans-serif; */
    font-size: 20px;
`