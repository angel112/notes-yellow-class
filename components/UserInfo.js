import { useState, useEffect } from 'react';
import styled from 'styled-components';
import { gql, useQuery } from '@apollo/client';
import { MdMail, MdAccessTime } from "react-icons/md";
//components
import Bone from './Bones';
//graphql
import { get_user } from '../graphql/queries';
//helper functions
import { evaluateTime } from './utils/timeCalculator';
import colors from './utils/colors';

const UserInfo = ({ userID }) => {
    //useState hooks
    const [userInfo, setUserInfo] = useState({});

    //graphql hooks
    const { data, loading } = useQuery(get_user, {
        variables: {
            userId: userID
        }
    });

    //useEffect hooks
    useEffect(() => {
        if (data) {
            setUserInfo(data.getUser);
        }
    }, [loading, setUserInfo]);


    return (userInfo && userInfo.createdAt ?
        <UserInfoContainer>
            <UserImage src={userInfo.picture} />
            <DetailsContainer>
                <WrapperName>{userInfo.username}</WrapperName>
                <MailWrapper><MdMail />{userInfo.email}</MailWrapper>
                <TimeStampWrapper><MdAccessTime />{evaluateTime(userInfo.createdAt)}</TimeStampWrapper>
            </DetailsContainer>
        </UserInfoContainer> : <Bone />
    )
}

export default UserInfo;

//styled components

const UserInfoContainer = styled.div`
    display: flex;
    align-items: center;
    flex-flow: row;
    flex-wrap: wrap;
    border-radius: 0.3rem;
    padding: 30px;
    margin: 5px;
    margin-left: 350px;
    max-width: 50%;
    justify-content: space-between;
    background: ${colors.neutral200};
`
const UserImage = styled.img`
    width: 200px;
    height: 200px;
    border-radius: 10rem;
    object-fit: contain;

`
const DetailsContainer = styled.div`
    display: flex;
    flex-flow: column;
    justify-content: space-around;
    margin-left: auto;

`
const WrapperName = styled.h1`
    color: ${colors.primary200};

`
const MailWrapper = styled.h2`
    color: ${colors.neutral400};
`
const TimeStampWrapper = styled.h2`
    color: ${colors.neutral400};
    margin-top: 8px;
    justify-self: flex-start;
    align-self: flex-start;
`