import React, { useState } from 'react'
import styled from 'styled-components';
import AuthUser from '../AuthUser'
import AuthenticateImage from '../../public/authenticate.svg';
import { useSpring, animated } from 'react-spring';
import colors from '../utils/colors';
import { MdClose } from 'react-icons/md';
import Loader from 'react-loader-spinner';


const AuthModal = ({ setUser, showModal, setShowModal }) => {
    const [loading, setLoading] = useState(false);
    const animation = useSpring({
        opacity: showModal ? 1 : 0,
        transform: showModal ? `translateY(0)` : `translateY(-30%)`
    })
    const onClose = () => {
        console.log("Closing!!!");
        setShowModal(false);
    }

    return (
        showModal ? <Container >
            {!loading ? <Wrapper style={animation}>
                <Close onClick={onClose} />
                {/* <Image src="/authModal.svg" width={500} height={400} /> */}
                <AuthImage />
                <Message>Please Login to Get Started with all of our Services!!</Message>
                <AuthUser setLoading={setLoading} setUser={setUser} showModal={showModal} setShowModal={setShowModal} />
            </Wrapper> :
                <Loading>
                    <Loader type="BallTriangle" color={`${colors.primary400}`} height={100} width={100} />
                    <h2>Signing In...</h2>
                </Loading>}
        </Container> : null)
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background: rgba(0,0,0,0.7);
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 100;
`

const Wrapper = styled(animated.div)`
    display: flex;
    flex-direction: column;
    width: 600px;
    background: white;
    border-radius: 0.2rem;
    padding: 1rem 3rem 3rem 1rem;
    align-items: center;
    filter: drop-shadow(1px 1px 9px #ffffff90);
`
const Close = styled(MdClose)`
    margin: 0.2rem 0.5rem;
    font-size: 2rem;
    align-self: flex-start;
    cursor: pointer;
`

const AuthImage = styled(AuthenticateImage)`
    transform: scale(0.5 0.5 0.5);
`
const Message = styled.h3`
    font-family: "IBM Plex Sans";
`

const Loading = styled(Wrapper)`
    min-height: 600px; 
    justify-content: center;
`

export default AuthModal;