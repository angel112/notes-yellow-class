import React from 'react'
import styled from 'styled-components';
import Form from '../Form';
import { MdClose } from 'react-icons/md';
import { useSpring, animated } from 'react-spring';
import typography from '../utils/typography';
import colors from '../utils/colors';


const FormModal = ({ showModal, setShowModal }) => {
    const animation = useSpring({
        opacity: showModal ? 1 : 0,
        transform: showModal ? `translateY(0)` : `translateY(-30%)`
    })
    const onClose = () => {
        console.log("Closing!!!");
        setShowModal(false);
    }
    return (
        showModal ? <Container>
            <Wrapper style={animation}>
                <Top>
                    <Header>Write a Post...</Header>
                    <Close onClick={onClose} />
                </Top>
                <Form />
            </Wrapper>
        </Container> : null)
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    background: ${colors.neutral200};
    width: 100vw;
    height: 100vh;
    max-height: 100vh;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 4;
    overflow-y: scroll;

`

const Wrapper = styled(animated.div)`
    display: flex;
    flex-direction: column;
    background: white;
    height: fit-content;
    padding: 1rem 1rem;
    border-radius: 1rem;
    margin-top: 48px;
    border: 1px solid ${colors.neutral300};
    border-radius: 0.2rem;
    @media (max-width: 700px) {
        width: 100vw;
        margin: 0;
        
    }
`
const Close = styled(MdClose)`
    align-self: flex-start;
    font-size: 2rem;
`

const Header = styled.h3`
    font-family: "Archivo Black", sans-serif;
    font-size: ${typography.h3};
`

const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    max-height: 70px;

`

export default FormModal;