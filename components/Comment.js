import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useSpring, config, animated } from 'react-spring';
import { useMutation } from '@apollo/client';
import { MdDelete } from 'react-icons/md';
//components
import Author from './Author';
import CommentLikeButton from './CommentLikeButton';
//helper functions
import checkAuth from './utils/auth-check';
import { DELETE_COMMENT } from '../graphql/mutations.js';
import colors from './utils/colors';

//component to display a single comment
const Comment = ({ comment }) => {
    //useState hooks
    const [likesCount, setLikesCount] = useState();
    const [liked, setLiked] = useState(false);
    const [showDeleteButton, setShowDeleteButton] = useState(false);
    const [deletes, setDelete] = useState(false);

    //useSpring hooks
    const remove = useSpring({
        transform: deletes ? `translateX(10%)` : `translateX(0)`,
        display: deletes ? `none` : 'flex',
        config: config.stiff
    });

    //useEffect hooks
    useEffect(() => {
        setLikesCount(comment.likeCount);
        let { response } = checkAuth(window.localStorage.getItem("yc_jwt"));
        if (response) {
            if (comment.likes.find(o => o.user == response.id)) {
                setLiked(true);
            }
            if (comment.user.id == response.id)
                setShowDeleteButton(true);
        }
    }, [setLiked, setShowDeleteButton]);

    //graphql hooks
    const [deleteComment] = useMutation(DELETE_COMMENT, {       //delete comment mutation
        variables: {
            deleteCommentParentId: comment.actionOn,
            deleteCommentCommentId: comment.id
        }
    });

    const handleCommentDeletion = async () => {
        try {
            setDelete(true);
            const res = await deleteComment();
            console.log("del post ", res);
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <CommentContainer style={remove}>
            <Author post={comment} />                   {/*displays author user details} */}
            <BodyWrapper>{comment.body}</BodyWrapper>
            <Actions>
                <CommentLikeButton likesCount={likesCount} setLikesCount={setLikesCount} liked={liked} setLiked={setLiked} postId={comment.id} />
                {showDeleteButton ?
                    <DeleteButton onClick={handleCommentDeletion} />
                    :
                    null}
            </Actions>
        </CommentContainer >
    )
}

export default Comment;

//styled-components

const CommentContainer = styled(animated.div)`
    align-items: left;
    display: flex;
    flex-flow: column;
    margin: 10px;
    padding: 10px;
    border-radius: 0.3rem;
    border: 1px ${colors.neutral300} solid;
`
const BodyWrapper = styled.h3`
    color: ${colors.neutral500};
    font-family: "IBM Plex Sans";
    font-weight: 400;
    margin: 2px;
`
const DeleteButton = styled(MdDelete)`
    color: ${colors.primary300};
    cursor: pointer;
    padding: 2px;
    align-self: flex-end;
    font-size: 1rem;
    border-radius: 4rem;
    transition: all ease-out 250ms;
    :hover {
        color: ${colors.neutral100};
        background: ${colors.primary300};
        filter: none;
    }
`

const Actions = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    margin-top: 1rem;
`