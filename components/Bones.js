import React from 'react'
import styled from 'styled-components';
import Skeleton from 'react-loading-skeleton';
//helper functions
import colors from './utils/colors';

//this is a loading react component that render when browser data is being fetched from the server
const Bone = () => (
    <PostContainer>
        <Skeleton circle={true} height={50} width={50} />
        <div style={{ padding: 10 }} />
        <Skeleton borderRadius={25} />
        <div style={{ padding: 10 }} />
        <Skeleton borderRadius={25} />
        <div style={{ padding: 10 }} />
        <Skeleton borderRadius={25} />
        <div style={{ padding: 10 }} />
        <Skeleton height={500} borderRadius={10} />
    </PostContainer>
)

export default Bone;

//styled-components

const PostContainer = styled.div`
    display: flex;
    flex-flow: column;
    margin: 10px;
    background: ${colors.neutral200};
    min-width: 800px;
    border-radius: 5px;
    padding: 1.2rem;
    z-index: 3;

    @media (max-width: 700px) {
        width: 100vw;
    }
`