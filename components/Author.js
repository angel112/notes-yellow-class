import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import Link from 'next/link';
import Skeleton from 'react-loading-skeleton';
//helper functions
import { evaluateTime } from './utils/timeCalculator';
import colors from './utils/colors';

const Author = ({ post }) => {
    //useState hooks
    const [user, setUser] = useState();     //user details hook

    //useEffect hooks
    useEffect(() => {                       // initializing user details with given props
        setUser(post.user);
    }, [setUser]);


    return (user ?
        <React.Fragment>
            <AuthorBody>
                <UserImage src={user.picture} alt="userImage" />
                <AuthorContents>
                    <Link href={`/user/${user.id}`} >
                        <User>{user.username}</User>
                    </Link>
                    <TimeStamp>{evaluateTime(post.createdAt)}</TimeStamp>
                </AuthorContents>
            </AuthorBody>
        </React.Fragment > :
        <Skeleton circle={true} height={50} width={50} />
    )
}

export default Author;

//styled-components
const AuthorBody = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 100%;
    padding-bottom: 1rem;
    border-bottom: ${colors.neutral300} solid 1px;
    align-self: flex-start;

`
const UserImage = styled.img`
    height: 32px;
    width: 32px;
    border-radius: 5rem;
`

const AuthorContents = styled.div`
    display: flex;
    flex-direction: column;
    align-content: flex-start;
    height: 30px;
    margin: 0 1rem;
`

const TimeStamp = styled.p`
    font-size: 0.5rem;
    color: grey;
    margin: 0;
    justify-self: flex-start;
    align-self: flex-start;
`

const User = styled.h3`
    margin: 0;  
    font-size: 1rem;
    cursor: pointer;
`