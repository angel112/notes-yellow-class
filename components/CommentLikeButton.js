import React from 'react';
import styled from 'styled-components';
import { useMutation } from '@apollo/client';
import { useSpring, animated } from 'react-spring';
import { useToasts } from 'react-toast-notifications';
//components
import Clap from "./Clap.js";
import Clapped from './Clapped.js';
//helper functions
import { LIKE_COMMENT } from '../graphql/mutations.js';
import checkAuth from './utils/auth-check'
import colors from './utils/colors.js';


const LikeButton = ({ setLikesCount, setLiked, likesCount, liked, postId }) => {
    //graphql hooks
    const [likeComment, { data, error }] = useMutation(LIKE_COMMENT, {
        variables: {
            parentId: postId
        }
    });

    //useSpring hooks
    const animate = useSpring({
        opacity: liked ? 1 : 0
    })
    const animateOut = useSpring({
        opacity: liked ? 0 : 1
    })

    //toast notifications hooks
    const { addToast } = useToasts();

    //methods
    const incrementLikes = async () => {
        const check = checkAuth(window.localStorage.getItem("yc_jwt"));     //extract current logged in user details
        if (check.error) {
            addToast("Please login to like", { appearance: 'warning' });
        } else {
            if (liked) {
                setLikesCount(likesCount - 1);
            } else {
                setLikesCount(likesCount + 1);
            }
            setLiked(!liked);
            try {
                await likeComment();
            } catch (err) {
                console.log(err);
            }
        }
    }

    return (
        <LikeButtonStyle>
            {!liked ? <ClapButton style={animateOut} onClick={incrementLikes}><Clap /></ClapButton> : null}
            {liked ? <ClapButton style={animate} onClick={incrementLikes}><Clapped /></ClapButton> : null}
            {typeof (likesCount) !== 'undefined' ? <p style={{ margin: 0 }}>{likesCount + " Claps"}</p> : <p>Loading..</p>}
        </LikeButtonStyle>
    )
}

export default LikeButton;

//styled-components

const LikeButtonStyle = styled.div`
    display: flex;
    align-items: center;
    padding: 0 12px 0 8px;
    border-radius: 0.4rem;
    cursor: pointer;
    :hover {
        background: ${colors.neutral200};
    }
`

const ClapButton = styled(animated.div)`
    height: 25px;
    width: 25px;
    padding: 5px;
    margin-right: 0.5rem;
`
