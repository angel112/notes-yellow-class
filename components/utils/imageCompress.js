import imageCompression from 'browser-image-compression';

export default async function compressImage(file) {
    console.log(file.size / 1024 / 1024);
    const options = {
        maxSizeMB: 2,
        maxWidthOrHeight: 600,
        fileType: 'image/jpeg'
    }

    const image = await imageCompression(file, options);
    console.log(image.size / 1024 / 1024);
    image.name = `${makeid(5)}.jpeg`;
    image.lastModified = new Date();
    console.log(image);
    const uri = await imageCompression.getDataUrlFromFile(image);
    return [image, uri];
};

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}