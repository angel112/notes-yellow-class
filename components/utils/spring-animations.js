import { config } from 'react-spring';

export const entryAnimation = (index) => {
    return {
        delay: index * 100,
        from: {
            opacity: 0,
            transform: `translateX(-100px)`
        },
        to: [
            {
                opacity: 1,
                transform: `translateX(0)`,
                config: config.wobbly
            }
        ]
    }
}
