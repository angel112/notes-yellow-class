const posts = [
    {
        postID: "0",
        username: "Angel Lakra",
        timestamp: new Date(2020, 5, 27, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_1.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "2",
        likedBy: ["dog", "cat"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "2"
            },
            {
                username: "Aaditya",
                text: ":)",
                likeCount: "10"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "2"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "5"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "0"
            }
        ]
    },
    {
        postID: "1",
        username: "Dexter Deshawne",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "1",
        likedBy: ["snake"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "1"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "2"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "2"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "2"
            }
        ]
    },
    {
        postID: "2",
        username: "Henry Smith",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "3",
        likedBy: ["peacock", "lion", "rat"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "2"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "3"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "1"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "0"
            }
        ]
    }, 
    {
        postID: "3",
        username: "Dexter Deshawne",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "4",
        likedBy: ["rat", "dog", "bat", "eagle"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "4"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "4"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "4"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "4"
            }
        ]
    }, 
    {
        postID: "4",
        username: "Dexter Deshawne",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "2",
        likedBy: ["shark", "dolphiin"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "4"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "4"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "4"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "4"
            }
        ]
    }, 
    {
        postID: "5",
        username: "Dexter Deshawne",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "3",
        likedBy: ["tiger", "elephant", "pig"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "4"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "4"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "4"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "4"
            }
        ]
    }, 
    {
        postID: "6",
        username: "Dexter Deshawne",
        timestamp: new Date(2020, 12, 10, 13, 30, 0).toDateString(),
        caption: "Exercitation🎈🎈 deserunt consectetur ut aliquip ipsum culpa laboris aliquip nostrud cillum irure dolor quis.",
        type: "image",
        media: "/image_2.jpg",
        hashtags: ["cat", "black", "photooftheday"],
        likeCount: "1",
        likedBy: ["crocodile"],
        comments: [
            {
                username: "Chaitanya",
                text: "I love yellow class",
                likeCount: "10"
            },
            {
                username: "Angel",
                text: "wow great",
                likeCount: "1"
            },
            {
                username: "Satyam",
                text: "yesterdays's class was amazing",
                likeCount: "6"
            },
            {
                username: "Neeraj",
                text: "Nice post",
                likeCount: "7"
            }
        ]
    }
]

export default function getPosts() {
    return posts;
}