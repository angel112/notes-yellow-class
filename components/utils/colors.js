const colors = {
    primary100: '#ffb61e',
    primary200: '#202C59',
    primary300: '#C20114',
    primary400: '#4CB944',
    primary500: '#DD5E98',
    neutral100: '#ffffff',
    neutral200: '#f4f5f7',
    neutral300: '#e1e1e1',
    neutral400: '#737581',
    neutral500: '#4a4b53',
    neutral600: '#000000',
    text: '#000000',
    textInverted: '#FFFFFF'
}

export default colors;