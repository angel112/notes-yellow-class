import jwt from 'jsonwebtoken';

export default function checkAuth(token) {
    let error = undefined;
    let response = undefined;
    if (token) {
        try {

            const res = jwt.verify(
                token,
                "some very secret key"
            );
            // console.log(res);
            response = res;
        } catch (err) {
            console.log(err);
            error = err;
        }
    } else {
        error = new Error("Unauthorized");
    }
    return {
        error, response
    }
}