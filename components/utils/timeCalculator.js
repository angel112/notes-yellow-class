export const evaluateTime = (clock) => {
    // console.log(clock);
    const date = clock.substring(0, 10).split('-');
    const time = clock.slice(11, 19).split(':');
    const timestamp = new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
    const newTime = new Date(timestamp.getTime() + 19800000);
    const mm = newTime.getMonth();
    const dd = newTime.getDate();
    const yy = newTime.getFullYear();
    const hh = newTime.getHours();
    const mn = newTime.getMinutes();
    // console.log(yy, mm, dd, hh, mn);


    // console.log(date, time);
    return `${hh < 10 ? `0${hh}` : hh}:${mn < 10 ? `0${mn}` : mn} | ${dd} ${month[mm + 1]} ${yy}`;
}

const month = {
    "1": "Jan",
    "2": "Feb",
    "3": "Mar",
    "4": "Apr",
    "5": "May",
    "6": "Jun",
    "7": "Jul",
    "8": "Aug",
    "9": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dec"
}

