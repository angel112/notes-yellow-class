export default {
    h1: "4.209rem",
    h2: "3.157rem",
    h3: "2.369rem",
    h4: "1.777rem",
    h5: "1.333rem",
    para: "1rem",
    helper: "0.8rem",
    copyright: "0.7rem"
}