import { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useMutation, useQuery } from '@apollo/client'
import { useToasts } from 'react-toast-notifications';
//components
import Comment from './Comment';
//helper functions
import typography from './utils/typography';
import colors from './utils/colors';
import checkAuth from './utils/auth-check';
//graphql
import { get_comments } from '../graphql/queries';
import { CREATE_COMMENT } from '../graphql/mutations';

const RenderComments = ({ postId, previousComments, setPreviousComments }) => {
    const { data, loading, error } = useQuery(get_comments, {
        variables: {
            postId: postId,
        }
    });

    useEffect(() => {
        if (data)
            setPreviousComments(data.getComments);
    }, [data, loading]);

    return (loading ? <p>Loading</p> : previousComments.comments ?
        previousComments.comments.map((comment, index) =>
            <Comment key={index} comment={comment} />
        ) : <p>No comments yet!</p>
    )
}

const CommentSection = ({ postId }) => {
    //useState hooks
    const [commentInput, setCommentInput] = useState("");
    const [previousComments, setPreviousComments] = useState([]);
    
    //graphql hooks
    const [createComment] = useMutation(CREATE_COMMENT);

    //toast hooks
    const { addToast } = useToasts();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const check = checkAuth(window.localStorage.getItem("yc_jwt"));
        setCommentInput("");
        if (check.error) {
            addToast("Please login to comment", { appearance: 'warning' });
        } else {
            try {
                await createComment({
                    variables: {
                        postId: postId,
                        body: commentInput
                    }
                });
                // if(data) {
                //     let arr = [data.createComment, ...previousComments];
                //     setPreviousComments(arr);
                // }
            } catch (err) {
                console.log(`Error occurerd: ${err}`);
            }
            window.location.reload();
        }
    }

    return (
        <CommentWrapper>
            <CommentFormWrapper>
                <CommentInput
                    autoComplete="off"
                    type="text"
                    name="comment"
                    value={commentInput}
                    onChange={e => setCommentInput(e.target.value)}
                    placeholder="Enter Comment"
                    onKeyPress={e => e.key == 'Enter' ? handleSubmit(e) : null}
                />
                <ButtonWrapper onClick={handleSubmit}>Comment</ButtonWrapper>
            </CommentFormWrapper>
            <RenderComments postId={postId} previousComments={previousComments} setPreviousComments={setPreviousComments} />
        </CommentWrapper>
    )
}

export default CommentSection;

//styled-components

const CommentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 0.5rem; 
    min-width: 50%;
    height: auto;
    max-height: 800px;
    overflow-y: scroll;
    ::-webkit-scrollbar-thumb {
        border-radius: 2rem;
        background: ${colors.neutral300};
    }
    ::-webkit-scrollbar {
        width: 0.5rem;
        background: none;
    }
`

const CommentFormWrapper = styled.div`
    display: flex;
    padding: 10px;
`

const ButtonWrapper = styled.button`
    border: none;
    font-size: ${typography.para};
    font-family: 'Nunito';
    border-radius: 0.3rem;    
    padding: 0.7rem 1.3rem;
    margin-left: 1rem;
    transition: all ease-in 300ms;
    background: ${colors.primary200};
    color: ${colors.textInverted};
    filter: drop-shadow(0 0.15rem 0.1rem rgba(0,0,0, 0.22));
    outline: none;
    cursor: pointer;
`

const CommentInput = styled.textarea`
    border: 1px solid ${colors.neutral300};
    border-radius: 0.3rem;
    width: 50%;
    height: 5rem;
    font-family: "IBM Plex Sans";
    outline: none;
    font-size: 1rem;
    
`