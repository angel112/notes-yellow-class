import React from 'react'
//helper functions
import styled from 'styled-components';
import colors from './utils/colors';

const Pagination = ({ onNext, more }) => {

    //methods
    const onTop = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }

    return (
        <PaginateContainer >
            <Button onClick={onTop}>Top</Button>
            <Button disabled={!more} onClick={onNext}>LoadMore</Button>
        </PaginateContainer>
    )
}

export default Pagination;

//styled-components

const PaginateContainer = styled.div`
    display: flex;
`

const Button = styled.button`
    background: ${colors.neutral500};
    color: ${colors.textInverted};
    outline: none;
    border: none;
    border-radius: 0.2rem;
    padding: 0.2rem 0.5rem;
    margin: 0.3rem 0.5rem;
    cursor: pointer;

    :disabled {
        color: ${colors.neutral400};
        cursor: not-allowed;
    }

`