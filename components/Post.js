import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Carousel from 'react-images';
import Link from 'next/link';
import { useSpring, animated, config } from 'react-spring';
import { useMutation } from '@apollo/client';
import { MdDelete } from 'react-icons/md';
//components
import LikeButton from '../components/LikeButton';
import Author from './Author';
//graphql
import { DELETE_POST } from '../graphql/mutations.js';
//helper functions
import checkAuth from './utils/auth-check';
import { entryAnimation } from './utils/spring-animations';
import colors from './utils/colors';
//assets
import CommentIcon from '../public/Comment.svg'



const Post = ({ item, i: index }) => {
    //useState hooks
    const [likesCount, setLikesCount] = useState();
    const [liked, setLiked] = useState(false);
    const [showDeleteButton, setShowDeleteButton] = useState(false);
    const [deletes, setDelete] = useState(false);
    const [animation, setAnimation] = useState(enter);

    //useSpring hooks
    const enter = useSpring(entryAnimation(index));     //the function with its arguement sets delay 
    //for each item in the list of posts

    const remove = useSpring({
        to: async (next) => {
            await next({
                transform: deletes ? `translateX(10%)` : `translateX(0)`,
                display: `none`,
                config: config.stiff
            })
        }
    });

    //graphql hooks
    const [deletePost, { data, error }] = useMutation(DELETE_POST, {
        variables: {
            deletePostPostId: item.id
        }
    });

    //useEffect hooks
    useEffect(() => {
        setLikesCount(item.likeCount);
        let { response } = checkAuth(window.localStorage.getItem("yc_jwt"));
        if (response) {
            if (item.user.id == response.id)
                setShowDeleteButton(true);
            let obj = item.likes.find(o => o.user == response.id);
            if (obj) {
                setLiked(true);
            }
        }
    }, [setLiked, setShowDeleteButton, setLikesCount]);

    //methods
    const images = (urls) => {
        const res = [];
        urls.map(url => {
            res.push({ source: url });
        })
        console.log(res);
        return res;
    }

    const handlePostDeletion = async () => {
        setAnimation(remove);
        try {
            setDelete(true);
            const res = await deletePost();
            console.log("del post ", res);
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <PostContainer style={animation}>
            <Author post={item} />
            <Body>
                <Link href={{
                    pathname: `/post/${item.id}`,
                }}>
                    <Caption>{item.body}</Caption>
                </Link>
                {item.imageUrls.length ?
                    <Media>
                        {/* <Photo src={item.imageUrls[0]} alt="image" /> */}
                        <Carousel
                            views={images(item.imageUrls)}
                        />
                    </Media> : null}
            </Body>
            <Hashtags>
                {item.hashtags.map((hashtag, index) =>
                    <Hashtag>#{hashtag}</Hashtag>
                )}
            </Hashtags>
            <Actions >
                <LikeButton setLiked={setLiked} setLikesCount={setLikesCount} likesCount={likesCount} liked={liked} postId={item.id} />
                <CommentI onClick={() => { window.location = `/post/${item.id}` }}>
                    <CommentIcon style={{ height: "25px", width: "25px" }} />
                    <p style={{ margin: "0 0 0 0.5rem" }}>Add Comment</p>
                </CommentI>
                {showDeleteButton ? <DeleteButton onClick={handlePostDeletion} /> : null}
            </Actions>
        </PostContainer>
    )
}

export default Post;

//styled-components

const PostContainer = styled(animated.div)`
    display: flex;
    flex-flow: column;
    width: 650px;
    margin: 10px 0;
    background: ${colors.neutral100};
    border-radius: 5px;
    padding: 1rem;
    z-index: 3;
    border: 1px solid ${colors.neutral300};

    @media (max-width: 700px) {
        width: 100vw;
        margin: 10px 0;
    }
`

const Caption = styled.p`
    margin-left: 48px;
    font-size: 20px;
`

const Media = styled.div`
    margin-left: 48px;
    display: flex;
    justify-content: center;
    align-items: flex-start;
`
const Body = styled.div`
    display: flex;
    flex-direction: column;
    cursor: pointer;
`
const Hashtags = styled.div`
    display: flex;
    flex-flow: row wrap;
    margin: 0.8rem 48px;
`
const Hashtag = styled.div`
    color: ${colors.neutral400};
    margin: 0 0.2rem;
`
const DeleteButton = styled(MdDelete)`
    color: ${colors.primary300};
    cursor: pointer;
    padding: 2px;
    align-self: flex-end;
    font-size: 2rem;
    border-radius: 4rem;
    transition: all ease-out 250ms;
    :hover {
        color: ${colors.neutral100};
        background: ${colors.primary300};
        filter: none;
    }
`

const Actions = styled.div`
    display: flex;
    margin-left: 48px;
`

const CommentI = styled.div`
    display: flex;
    align-items: center;
    padding: 0 12px 0 8px;
    border-radius: 0.4rem;
    :hover {
        background: ${colors.neutral200};
    }
`
