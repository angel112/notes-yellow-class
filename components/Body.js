import React from 'react';
import styled from 'styled-components';
//components
import MiddleColumn from './MiddleColumn';
//helper functions
import colors from './utils/colors';

const Body = () => {
    return (
        <React.Fragment>
            <BodyContainer>
                <MiddleColumn />
            </BodyContainer>
        </React.Fragment>
    )
}

export default Body;

//styled-components

const BodyContainer = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: row;
    background: ${colors.neutral200};
    padding: 56px 0;
    z-index: 2;
    min-height: 100vh;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
`