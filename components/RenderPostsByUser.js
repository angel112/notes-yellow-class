import { useState, useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { get_posts_by_user } from '../graphql/queries';
import Bone from './Bones';
import Post from './Post';
import styled from 'styled-components';

const RenderPostsByUser = ({ userID }) => {
    //useState hooks
    const [posts, setPosts] = useState([]);

    //graphql hooks
    const { data, loading } = useQuery(get_posts_by_user, {
        variables: {
            userId: userID,
            pageSize: 10
        }
    });

    //useEffect hooks
    useEffect(() => {
        if (data) {
            setPosts(data.getPostsByUser);
        }
    }, [data, loading]);


    return (
        <Container>
            {posts && posts.posts ?
                posts.posts.map((post, index) => <Post item={post} key={index} i={index} />) : <Bone />}
        </Container>
    )

}

export default RenderPostsByUser;

//styled-components

const Container = styled.div`
    display: flex;
    flex-flow: column;
    flex-basis: 45%;
    justify-content: center;
    margin-left: 350px;
`