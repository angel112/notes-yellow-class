import React from 'react'
import Link from 'next/link'
import styled from 'styled-components';
//helper functions
import colors from './utils/colors';

export default function Title() {
    return (
        <Logo>
            <Link href="/">
                Yellow
            </Link>
            <Link href="/">
                <span>Class</span>
            </Link>
        </Logo>
    )
}

//styled-components

const Logo = styled.div`
    /* border: 1px solid red; */
    font-size: 1.777rem;
    color: ${colors.primary100};

    cursor: pointer;

    & > span {
            color: black;
        font-size: 1.3rem;
    }
`