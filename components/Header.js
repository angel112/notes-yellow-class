import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import jwt from 'jsonwebtoken';
import { FiEdit } from 'react-icons/fi'
//components
import FormModal from './modals/FormModal';
import AuthModal from './modals/AuthModal';
import Title from './Title';
import LoggedIn from './LoggedIn';
//helper functions
import typography from './utils/typography';
import colors from './utils/colors'


const Header = () => {
    //useState hooks
    const [w, setW] = useState();
    const [showPostModal, setShowPostModal] = useState(false);
    const [showAuthModal, setShowAuthModal] = useState(false);
    const [user, setUser] = useState('');

    //useEffect hooks
    useEffect(() => {
        setW(window.innerWidth);
        const token = window.localStorage.getItem("yc_jwt");
        let data = undefined;
        try {
            data = jwt.verify(token, "some very secret key");
        } catch (err) {
            if (err.name == "JsonWebTokenError") logout();
        }
        setUser(data);
    }, [setUser]);

    //methods
    const initiateForm = () => {
        if (user) {
            setShowPostModal(true);
        }
        else {
            setShowAuthModal(true);
        }
    }

    const openAuthModal = (e) => {
        setShowAuthModal(true);
    }

    const logout = () => {
        window.localStorage.removeItem("yc_jwt");
        setUser();
    }


    return (
        <Nav>
            <Container>
                <Title />
                <Authenticate user={user}>
                    <NewPost onClick={initiateForm}>
                        {w && w > 700 ? "Write a Post" : <FiEdit />}
                    </NewPost>
                    {!user ?
                        <Login onClick={openAuthModal}>Login</Login> :
                        <LoggedIn Logout={Logout} logoutUser={setUser} userId={user} />}
                </Authenticate>
                <FormModal showModal={showPostModal} setShowModal={setShowPostModal} />
                <AuthModal setUser={setUser} showModal={showAuthModal} setShowModal={setShowAuthModal} />
            </Container>
        </Nav>
    );
};

export default Header;

//styled-components

const Nav = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: visible;
    position: fixed;
    left: 0;
    top: 0;
    z-index: 100;
    height: 56px;
    width: 100vw;
    background: white;
    filter: drop-shadow(0px 1px 1px rgba(0, 0, 0, 0.1)); 
`

const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 56px;
    width: 1280px;
    padding: 1rem;
    /* border: 1px black solid; */
    
`;

const Button = styled.button`
    border: none;
    font-size: ${typography.para};
    font-family: 'Nunito';
    border-radius: 0.2rem;    
    padding: 0.7rem 1rem;
    transition: all ease-in 300ms;
    filter: drop-shadow(0 0.15rem 0.1rem rgba(0,0,0, 0.22));
    outline: none;
    cursor: pointer;

    :hover {
        color: white;
        background: black;
        filter: none;
    }

    @media (max-width: 700) {
        padding: 0.3rem 0.5rem;
    }
`

const Login = styled(Button)`
    background: ${colors.primary400};
    color: ${colors.textInverted};  
    margin-left: 0.5rem;
`;

const NewPost = styled(Button)`
    /* flex-basis: 10%; */
    background: ${colors.primary200};
    color: ${colors.textInverted};
`;

const Logout = styled(Button)`
    background: ${colors.primary300};
    color: ${colors.textInverted};
    margin-left: 0.5rem;
`;

const Authenticate = styled.div`
    display: flex;  
    max-width: ${props => props.user ? "280px" : "250px"};
    justify-content: space-between;
    @media (max-width: 700px) {
        justify-content: flex-end;    
    }
/* border: 1px green solid; */
`