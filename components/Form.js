import React, { useState, useEffect, Fragment } from 'react'
import styled from 'styled-components';
import { useMutation } from '@apollo/client';
import Loader from 'react-loader-spinner';
import Dropzone from 'react-dropzone';
//graphql
import { CREATE_POST, UPLOAD_FILE } from '../graphql/mutations';
//helper functions
import typography from './utils/typography';
import colors from './utils/colors';
import compressImage from './utils/imageCompress';

const Form = () => {
    //useState hooks
    const [image, setImage] = useState([[], []]);
    const [caption, setCaption] = useState("");
    const [hashtag, setHashtag] = useState("");
    const [hashtagArray, setHashtagArray] = useState([]);
    const [uploading, setUploading] = useState(false);

    //graphql hooks
    const [createPost] = useMutation(CREATE_POST);
    const [uploadFile, { loading }] = useMutation(UPLOAD_FILE);

    //methods
    const clearStates = () => {
        setImage("");
        setCaption("");
        setHashtagArray([]);
    }

    const handleSubmit = async e => {
        e.preventDefault();
        setUploading(true);
        try {
            const images = [];
            console.log(image);
            if (image[0].length) {
                await image[0].map(async pic => {
                    const imageUri = await uploadFile({
                        variables: {
                            file: pic
                        }
                    })
                    const url = imageUri.data.uploadFile.uri;
                    images.push(url);
                })
                setTimeout(async () => {
                    await createPost({
                        variables: {
                            body: caption,
                            imageUrls: images.length ? images : null,
                            hashtags: hashtagArray
                        }
                    })
                    window.location = '/';
                }, 8000);
            }
        } catch (err) {
            console.log(err);
            window.localStorage.removeItem("yc_jwt");
        }
    }

    const addHashtag = e => {
        e.preventDefault();
        const hArray = hashtagArray;
        if (hashtag != '') hArray.push(hashtag.replace(/\s/g, ''));
        setHashtagArray(hArray);
        setHashtag("");
    }

    const handleDrop = async (files) => {
        if (image[0].length < 10) {
            console.log(files);
            const res = await compressImage(files[0]);
            const img = [...image];
            img[0].push(res[0]);
            img[1].push(res[1]);
            console.log("img", img);
            setImage(img);
        }
    }

    return (
        !uploading ? (<Fragment>
            <FormContainer>
                <Dropzone
                    accept="image/*,video/*"
                    maxFiles={1}
                    onDrop={handleDrop}
                >
                    {
                        ({ getRootProps, getInputProps }) => (
                            <DragDrop {...getRootProps()}>
                                <input {...getInputProps()} />
                                {image[1][0] ? image[1].map((img, i) => <img key={i} src={img} style={{ maxHeight: "100px", objectFit: "contain", marginRight: "0.5rem" }} />) :
                                    <h3>Drag and Drop or Click to Browse Images!</h3>}
                            </DragDrop>
                        )
                    }
                </Dropzone>
                <Caption type="text"
                    placeholder="Enter your caption here"
                    value={caption}
                    onChange={e => setCaption(e.target.value)}
                />
                <HashtagInput
                    type="text"
                    value={hashtag}
                    placeholder="Add a suitable hashtag"
                    onChange={e => setHashtag(e.target.value)}
                    onKeyPress={e => e.key == 'Enter' ? addHashtag(e) : null}
                />
                <HashtagButton type="button" onClick={addHashtag}>Add hashtag</HashtagButton>
                <Hashtags>
                    {hashtagArray.map((item, index) => (<Hashtag key={index}>#{item}</Hashtag>))}
                </Hashtags>
                <SubmitButton onClick={handleSubmit}>Create Post</SubmitButton>
                <ResetButton type="reset" onClick={clearStates}>Reset</ResetButton>
            </FormContainer>
        </Fragment>) :
            <LoadingSplash>
                <Loader type="Circles" color="#00BFFF" height={80} width={80} />
                <h3>Uploading</h3>
            </LoadingSplash>
    )
}

export default Form;


//styled-components

const FormContainer = styled.div`
    width: 100%;
    font-size: 'Work Sans', sans-serif;
`
const Button = styled.button`
        border: none;
        outline: none;
        font-size: ${typography.para};
        border-radius: 0.2rem;
        padding: 3px 10px;
        filter: drop-shadow(3px 4px 2px #52521266);
        transition: all ease-out 300ms;
        font-family: "Nunito";
        :hover {
            filter: none;
            transform: translate(1px, 1px);
        }
`

const Caption = styled.textarea`
    margin-top: 2rem;
    min-height: 400px;
    min-width: 100%;
    border: none;
    padding-top: 0.5rem;
    border-top: 1px solid ${colors.neutral400};
    border-bottom: 1px solid ${colors.neutral400};
    outline: none;
    font-size: ${typography.h4};
    transition: all ease-in-out 100ms;
`

const HashtagInput = styled.input`
    border: none;
    outline: none;
    font-size: ${typography.h5};
    font-family: 'Work Sans', sans-serif;
`

const HashtagButton = styled(Button)`
    background: ${colors.primary200};
    color: ${colors.textInverted}
`
const SubmitButton = styled(Button)`
    background: ${colors.primary400};
    margin: 1rem;
`
const ResetButton = styled(Button)`
    background: ${colors.primary300};
    margin: 0.5rem;
    color: ${colors.textInverted}
`

const Hashtags = styled.div`
    display: flex;
    flex-wrap: wrap;

`
const Hashtag = styled.div`
    font-size: 0.8rem;
    margin: 0.5rem 0.2rem;
    padding: 0.1rem;
`

const DragDrop = styled.div`
    border: solid ${colors.neutral400} 4px;
    min-height: 10rem;
    border-radius: 1rem;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    background: ${colors.neutral200};
    color: ${colors.neutral400};
    cursor: pointer;
    outline: none;
    padding-left: 0.5rem;
`
const LoadingSplash = styled.div`
    display: flex;
    flex-direction: column;
    align-self: center;
`

