import React from 'react';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';
import { useMutation } from '@apollo/client';
import { useToasts } from 'react-toast-notifications';
//components
import Clap from "./Clap.js";
import Clapped from './Clapped.js';
//graphql
import { LIKE_POST } from '../graphql/mutations.js';
//helper functions
import checkAuth from './utils/auth-check'
import colors from './utils/colors.js';


const LikeButton = ({ setLikesCount, setLiked, likesCount, liked, postId }) => {
    //graphql hooks
    const [likePost, { data, error }] = useMutation(LIKE_POST, {
        variables: {
            parentId: postId
        }
    });

    //useSpring hooks
    const animate = useSpring({
        opacity: liked ? 1 : 0
    })

    const animateOut = useSpring({
        opacity: liked ? 0 : 1
    })
    //toast notification hook
    const { addToast } = useToasts();

    //methods
    const incrementLikes = async () => {
        const check = checkAuth(window.localStorage.getItem("yc_jwt"));
        console.log("checked", check);
        if (check.error) {
            // alert('Please Login!!');
            addToast("Please login to like", { appearance: 'warning' });
        } else {
            if (liked) {
                setLikesCount(likesCount - 1);
            } else {
                setLikesCount(likesCount + 1);
            }
            setLiked(!liked);
            try {
                const res = await likePost();
                console.log("Result", res);
            } catch (err) {
                console.log(err);
            }
        }
    }

    return (
        <LikeButtonStyle>
            {!liked ? <ClapButton style={animateOut} onClick={incrementLikes}><Clap /></ClapButton> : null}
            {liked ? <ClapButton style={animate} onClick={incrementLikes}><Clapped /></ClapButton> : null}
            {typeof (likesCount) !== 'undefined' ? <p style={{ margin: 0 }}>{likesCount + " Claps"}</p> : <p>Loading..</p>}
        </LikeButtonStyle>
    )
}

export default LikeButton;

//styled-components

const LikeButtonStyle = styled.div`
    display: flex;
    align-items: center;
    padding: 0 12px 0 8px;
    border-radius: 0.4rem;
    cursor: pointer;


    :hover {
        background: ${colors.neutral200};
    }
`

const ClapButton = styled(animated.div)`
    height: 35px;
    width: 35px;
    padding: 5px;
    margin-right: 0.5rem;
`
