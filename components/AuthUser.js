import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { useGoogleLogin, useGoogleLogout } from 'react-google-login';
import { useMutation } from '@apollo/client'
import { FcGoogle } from "react-icons/fc";
//helper functions
import { LOGIN } from '../graphql/mutations';
import colors from './utils/colors';

const clientId = process.env.NEXT_PUBLIC_CLIENTID;      //google oauth client id

const AuthUser = ({ setLoading, setUser, showModal, setShowModal }) => {
    //graphql hooks
    const [user, { loading }] = useMutation(LOGIN);     //login graphql mutation   

    //useEffect Hooks
    useEffect(() => {
        if (loading) {
            setLoading(true);                           //function passed as props to display loading spinner in authModal
        }
    }, [loading]);
    const onSuccess = async (res) => {                  //callback when oauth is successful
        console.log('[Login Success] User: ', res);
        const googleID = res.profileObj.googleId;
        try {
            const { data } = await user({
                variables: {
                    google_id: googleID,
                    accessToken: res.accessToken
                }
            });
            localStorage.setItem("yc_jwt", data.login.token);   //store jwt in localstorage 
        } catch {
            console.log("Some error happened!!");
        }
        signOut();                                              //delete google service cookies from browser
        window.location.reload();                               //return to previous page by reload
    }

    const onFailure = (res) => {                                //callback on failed oauth procedure
        console.log('[Login Failed] Error:', res);
    }


    const { signIn } = useGoogleLogin({                         //hook that provides signin function
        onSuccess,
        onFailure,
        clientId,
        isSignedIn: true,
        accessType: 'offline',
    });

    const { signOut } = useGoogleLogout({                       //hook that provides signout function
        onFailure,
        onLogoutSuccess: () => {
            // console.log("Logged Out!!");
        },
        clientId
    })

    return (
        <GoogleButton onClick={signIn}>
            <h5>{"Login with  "}</h5>
            <Icon />
        </GoogleButton>
    )
}

export default AuthUser;

//styled-components

const GoogleButton = styled.button`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    width: 150px;
    height: 50px;
    background: ${colors.primary200};
    color: ${colors.textInverted};
    border: none;
    font-size: 1rem;
    font-family: 'Nunito';
    border-radius: 0.2rem;    
    padding: 0.7rem 1.3rem;
    margin-left: 1rem;
    transition: all ease-in 300ms;
    outline: none;
    cursor: pointer;
    /* filter: drop-shadow(5px 1px 4px #111111); */
    :hover {
        /* color: ${colors.text}; */
        background: ${colors.neutral600};
        filter: drop-shadow(5px 1px 4px #11111180);
    }
`

const Icon = styled(FcGoogle)`
    font-size: 1.5rem;
`
